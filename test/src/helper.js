const {spawn} = require("child_process");

module.exports.execute = (flags, template = "template") =>
    startExpress().then((server) => {
        const address = server.address();
        const host = address.address;
        const port = address.port;
        const childProcess = spawn("node", ["index.js", "--url", `http://${host}:${port}`, "--template", `http://${host}:${port}/${template}`, ...flags], {
            timeout: 10000
        });

        return new Promise((resolve) => {
            let stdout = "";
            childProcess.stdout.on('data', (data) => stdout += data);
            childProcess.stderr.on('data', (data) => console.error(`${data}`));
            childProcess.on('close', () => {
                server.close();
                resolve(stdout)
            })
        })
    })

function startExpress() {
    return new Promise((resolve) => {
        const express = require("express");
        const app = express();
        app.use('/', express.static(__dirname + '/../mock'));

        const server = app.listen(6000, '127.0.0.1', () => resolve(server))
    })
}

module.exports.buildExpected = lines => lines.join("\n") + "\n";