const {execute, buildExpected} = require("./helper");
jest.setTimeout(20000);
describe("With limited count", () => {
    it("IssueComments", async () => {
        const expected = buildExpected([
            "<!--RECENT_ACTIVITY:start-->",
            "🗨️ Commented on Issue [#349292](https://gitlab.com/gitlab-org/gitlab/-/issues/349292#note_795826076) in [GitLab.org / GitLab](https://gitlab.com/gitlab-org/gitlab)",
            "<!--RECENT_ACTIVITY:end-->"
        ]);

        const result = await execute(["--user", "IssueComments", "--count", "1"]);
        expect(result).toEqual(expected)
    })

    it("IssueComments Shorthand", async () => {
        const expected = buildExpected([
            "<!--RECENT_ACTIVITY:start-->",
            "🗨️ Commented on Issue [#349292](https://gitlab.com/gitlab-org/gitlab/-/issues/349292#note_795826076) in [GitLab.org / GitLab](https://gitlab.com/gitlab-org/gitlab)",
            "<!--RECENT_ACTIVITY:end-->"
        ]);

        const result = await execute(["--user", "IssueComments", "-c", "1"]);
        expect(result).toEqual(expected)
    })
})