const {execute, buildExpected} = require("./helper");
jest.setTimeout(20000);
describe("With collapsed comments", () => {
    it("IssueComments", async () => {
        const expected = buildExpected([
            "<!--RECENT_ACTIVITY:start-->",
            "🗨️ Commented 2 times on Issue [#349292](https://gitlab.com/gitlab-org/gitlab/-/issues/349292#note_795819980) in [GitLab.org / GitLab](https://gitlab.com/gitlab-org/gitlab)",
            "<!--RECENT_ACTIVITY:end-->"
        ]);

        const result = await execute(["--user", "IssueComments", "--collapse-comments"]);
        expect(result).toEqual(expected)
    })
})