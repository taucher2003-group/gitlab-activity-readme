const {execute, buildExpected} = require("./helper");
jest.setTimeout(20000);
describe("Keeps additional data around the section", () => {
    it("IssueComments", async () => {
        const expected = buildExpected([
            "## Some Content",
            "",
            "<!--RECENT_ACTIVITY:start-->",
            "🗨️ Commented on Issue [#349292](https://gitlab.com/gitlab-org/gitlab/-/issues/349292#note_795826076) in [GitLab.org / GitLab](https://gitlab.com/gitlab-org/gitlab) \\",
            "🗨️ Commented on Issue [#349292](https://gitlab.com/gitlab-org/gitlab/-/issues/349292#note_795819980) in [GitLab.org / GitLab](https://gitlab.com/gitlab-org/gitlab)",
            "<!--RECENT_ACTIVITY:end-->",
            "",
            "### Some more Content"
        ]);

        const result = await execute(["--user", "IssueComments"], "template-filled");
        expect(result).toEqual(expected)
    })
})