const {execute, buildExpected} = require("./helper");
jest.setTimeout(20000);
describe("With GitLab Mention As Link", () => {
    it("IssueComments", async () => {
        const expected = buildExpected([
            "<!--RECENT_ACTIVITY:start-->",
            "🗨️ Commented on Issue [#349292](gitlab-org/gitlab#349292) in [GitLab.org / GitLab](https://gitlab.com/gitlab-org/gitlab) \\",
            "🗨️ Commented on Issue [#349292](gitlab-org/gitlab#349292) in [GitLab.org / GitLab](https://gitlab.com/gitlab-org/gitlab)",
            "<!--RECENT_ACTIVITY:end-->"
        ]);

        const result = await execute(["--user", "IssueComments", "--use-gitlab-mention-as-link"]);
        expect(result).toEqual(expected)
    })
})