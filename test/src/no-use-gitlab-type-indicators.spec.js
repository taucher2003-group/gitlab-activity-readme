const {execute, buildExpected} = require("./helper");
jest.setTimeout(20000);
describe("Without GitLab Type indicators", () => {
    it("IssueComments", async () => {
        const expected = buildExpected([
            "<!--RECENT_ACTIVITY:start-->",
            "🗨️ Commented on Issue [349292](https://gitlab.com/gitlab-org/gitlab/-/issues/349292#note_795826076) in [GitLab.org / GitLab](https://gitlab.com/gitlab-org/gitlab) \\",
            "🗨️ Commented on Issue [349292](https://gitlab.com/gitlab-org/gitlab/-/issues/349292#note_795819980) in [GitLab.org / GitLab](https://gitlab.com/gitlab-org/gitlab)",
            "<!--RECENT_ACTIVITY:end-->"
        ]);

        const result = await execute(["--user", "IssueComments", "--no-use-gitlab-type-indicators"]);
        expect(result).toEqual(expected)
    })
})