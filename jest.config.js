module.exports = {
    testMatch: ["**/*.spec.js"],
    transformIgnorePatterns: [
        "/node_modules/(?!node-fetch)",
    ]
}
