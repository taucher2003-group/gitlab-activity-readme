const fetch = require("node-fetch");
const commandLineArgs = require("command-line-args");

const optionDefinitions = [
    { name: 'action', alias: 'a', type: String, multiple: true },
    { name: 'target', alias: 't', type: String, multiple: true },
    { name: 'url', type: String },
    { name: 'user', alias: 'u', type: String },
    { name: 'count', alias: 'c', type: Number },
    { name: 'template', type: String },
    { name: 'output', type: String },
    { name: 'line-seperator', type: String },
    { name: 'use-gitlab-mention', type: Boolean },
    { name: 'use-gitlab-mention-as-link', type: Boolean },
    { name: 'no-use-gitlab-type-indicators', type: Boolean },
    { name: 'collapse-comments', type: Boolean }
];

const programOptions = commandLineArgs(optionDefinitions);

const ACTION_MESSAGES = {
    "approved": "✔️ Approved Merge Request {url} in {location}",
    "closed": "✔️ Closed {type} {url} in {location}",
    "commented on": "🗨️ Commented on {type} {url} in {location}",
    "commented multiple on": "🗨️ Commented {amount} times on {type} {url} in {location}", // custom action
    "opened": "📬 Opened {type} {url} in {location}", // not an event filter
    "created": "🚀 Created {location}",
    "imported": "📥 Imported {location}",
    "destroyed": "🗑️ Deleted {location}",
    "joined": "👥 Joined {location}",
    "left": "👤 Left {location}",
    "merged": "🎉 Merged Merge Request {url} in {location}",
    "accepted": "🎉 Accepted Merge Request {url} in {location}", // same as merged, but action is different in the API
    "pushed": "📥 Pushed {url} in {location}",
    "reopened": "📬 Reopened {type} {url} in {location}",
    "updated": "🔧 Updated {type} {url} in {location}"
};

const DEFAULT_ACTIONS = [
    "approved",
    "closed",
    "commented",
    "created",
    "destroyed",
    "joined",
    "left",
    "merged",
    // "pushed", -- we exclude pushes by default
    "reopened",
    "updated"
];

const useTargetFilter = programOptions.target && programOptions.target.length;
const useActions = programOptions.action && programOptions.action.length ? programOptions.action : DEFAULT_ACTIONS;
const gitlabUrl = programOptions.url ?? process.env.CI_SERVER_URL;
const targetUser = programOptions.user ?? process.env.GITLAB_USER_LOGIN;
const maxEvents = programOptions.count ?? 5;
const output = programOptions.output ?? "STDOUT";
const lineSeperator = (programOptions['line-seperator'] ?? " \\\n").replace("\\n", "\n");
const useGitLabMentionAsLink = programOptions['use-gitlab-mention-as-link'] ?? false;
const useGitLabMention = programOptions['use-gitlab-mention'] || useGitLabMentionAsLink;
const useGitLabTypeIndicators = !(programOptions['no-use-gitlab-type-indicators'] ?? false);
const useDeduplication = programOptions['collapse-comments'] ?? false;

const EVENTS_API_ENDPOINT = "/api/v4/users/{id}/events";
const PROJECT_API_ENDPOINT = "/api/v4/projects/{id}";

const activities = [];

function executeFetch(url, acceptableCodes = [200], retriesLeft = 2) {
    const retry = () => executeFetch(url, acceptableCodes, retriesLeft - 1)

    return fetch(url)
        .then(response => {
            if(acceptableCodes.includes(response.status) || retriesLeft === 0) {
                return { response, url };
            }
            return retry();
        })
        .catch((e) => {
            if(retriesLeft > 0) {
                return retry();
            }
            throw e;
        });
}

const fetchPromises = useActions.map(async activity => {
    const {response, url} = await executeFetch(`${gitlabUrl}${EVENTS_API_ENDPOINT.replace("{id}", targetUser)}?action=${activity}`);
    if(response.status !== 200) {
        return Promise.reject(`GitLab returned response code ${response.status} for ${url}`);
    }

    const body = await response.json();
    // Events API returns an array of activity entries
    body.forEach(entry => {
        if(!activities.map(a => a.id).includes(entry.id)) {
            activities.push(entry)
        }
    });
});

Promise.all(fetchPromises).then(() => {
    activities.sort((a, b) => a.id > b.id ? -1 : 1);
    let counter = 0;
    const deduplicatedEvents = [...activities].filter((val, index) => {
        if(!useDeduplication) { // don't use deduplication without the flag
            return true;
        }

        // noinspection JSUnresolvedVariable
        if(val.action_name === "commented on") {
            const activityAfter = activities[index + 1];
            // noinspection JSUnresolvedVariable
            if(
                activityAfter !== undefined
                && activityAfter.action_name === val.action_name
                && activityAfter.project_id === val.project_id
                && activityAfter.note.noteable_id === val.note.noteable_id
            ) {
                counter++;
                return false;
            }
            if(counter > 0) {
                val.action_name = "commented multiple on";
                val.custom = val.custom || {};
                val.custom.counter = counter + 1;
                counter = 0;
                return true;
            }
        }
        return true;
    })
    const lastEvents = deduplicatedEvents.filter((_, index) => index < maxEvents);

    Promise.all(lastEvents.map(convertToMessage))
        .then(replaceActivityMessages)
        .then(result => {
            if(output === "STDOUT") {
                console.log(result);
            }
        })
    ;
});

async function replaceActivityMessages(messages) {
    const source = await getTemplateSource();
    return source.replace(
        /<!--RECENT_ACTIVITY:start-->[\s\S]*<!--RECENT_ACTIVITY:end-->/,
        `<!--RECENT_ACTIVITY:start-->\n${messages.join(lineSeperator)}\n<!--RECENT_ACTIVITY:end-->`
    );
}

async function getTemplateSource() {
    // noinspection JSUnresolvedVariable
    if(programOptions.template) {
        const {response} = await executeFetch(`${programOptions.template}`);
        return response.text();
    }

    // noinspection JSUnresolvedVariable
    const {response} = await executeFetch(`${await getProject(process.env.CI_PROJECT_ID).readme_url}`);
    return response.text();
}

async function convertToMessage(event) {
    // noinspection JSUnresolvedVariable
    const project = await getProject(event.project_id);

    // noinspection JSUnresolvedVariable
    const actionName = event.action_name;

    // noinspection JSUnresolvedVariable
    const name = project.name_with_namespace;
    // noinspection JSUnresolvedVariable
    const projectUrl = project.web_url;
    // noinspection JSUnresolvedVariable
    const path = project.path_with_namespace;

    // noinspection JSUnresolvedVariable
    const internalTypeName = event.target_type in typeMappingResolver
        ? typeMappingResolver[event.target_type](event)
        : event.target_type;

    // noinspection JSUnresolvedVariable
    const typeName = internalTypeName in typeMappings
        ? typeMappings[internalTypeName]
        : internalTypeName;

    // noinspection JSUnresolvedVariable
    const eventItemId = event.target_type in typeIdResolver
        ? typeIdResolver[event.target_type](event)
        : event.target_iid;

    // noinspection JSUnresolvedVariable
    const eventItemUrl = internalTypeName in urlMappings
        ? `${projectUrl}${urlMappings[internalTypeName].replace("{id}", eventItemId)}`
        : `${projectUrl}/${internalTypeName}/${eventItemId}`;

    // noinspection JSUnresolvedVariable
    const eventItemUrlAppendix = event.target_type in urlAppendixes
        ? `${urlAppendixes[event.target_type](event)}`
        : "";

    const typeIndicatorName = useGitLabTypeIndicators
        ? internalTypeName in typeIndicatorMappings
            ? `${typeIndicatorMappings[internalTypeName]}${eventItemId}`
            : `${eventItemId}`
        : `${eventItemId}`;

    const urlReplacement = useGitLabMention
        ? useGitLabMentionAsLink
            ? `[${typeIndicatorName}](${path}${typeIndicatorName})`
            : `${path}${typeIndicatorName}`
        : `[${typeIndicatorName}](${eventItemUrl}${eventItemUrlAppendix})`;

    if(!(actionName in ACTION_MESSAGES)) {
        return `*Unknown interaction (${actionName})*`;
    }

    // noinspection JSUnresolvedVariable
    return ACTION_MESSAGES[actionName]
        .replace("{amount}", event.custom?.counter)
        .replace("{type}", typeName)
        .replace("{url}", urlReplacement)
        .replace("{location}", `[${name}](${projectUrl})`);
}

const projectCache = {};

async function getProject(id) {
    if(id in projectCache) {
        return projectCache[id];
    }

    const {response, url} = await executeFetch(`${gitlabUrl}${PROJECT_API_ENDPOINT.replace("{id}", id)}`);
    if(response.status !== 200) {
        return Promise.reject(`GitLab returned response code ${response.status} for ${url}`);
    }

    const body = response.json();
    // noinspection JSUnresolvedVariable
    projectCache[id] = body;
    return body;
}

const typeMappings = {
    'MergeRequest': 'Merge Request',
};

const typeIndicatorMappings = {
    'MergeRequest': '!',
    'Issue': '#',
    'Epic': '&',
    'Milestone': '%'
}

const urlMappings = {
    'MergeRequest': '/-/merge_requests/{id}',
    'Issue': '/-/issues/{id}'
};

// noinspection JSUnresolvedVariable
const typeMappingResolver = {
    'DiffNote': (event) => event.note.noteable_type,
    'Note': (event) => event.note.noteable_type,
    'DiscussionNote': (event) => event.note.noteable_type
}

// noinspection JSUnresolvedVariable
const typeIdResolver = {
    'DiffNote': (event) => event.note.noteable_iid,
    'Note': (event) => event.note.noteable_iid,
    'DiscussionNote': (event) => event.note.noteable_iid
}

// noinspection JSUnresolvedVariable
const urlAppendixes = {
    'DiffNote': (event) => `#note_${event.note.id}`,
    'Note': (event) => `#note_${event.note.id}`,
    'DiscussionNote': (event) => `#note_${event.note.id}`,
}
