FROM node:16.11.0-buster AS build
COPY package.json package.json
COPY package-lock.json package-lock.json
RUN npm ci

FROM node:16.11.0-buster
COPY --from=build node_modules/ node_modules/
ADD index.js index.js
CMD ["node", "index.js"]
